#NpKnob

##About
A small pure javascript library for creating rotating knobs. It was created while I had the idea to rebuild my BeatMachine.

##Usage
Create an element with an id. Then add the next code:

    var knob1 = new NpKnob('your-id', options);

And thats it. Now you can listen to the events it's fyering:

    knob1.obj.addEventListener('knob-rotate', function (evt) {
        /**
         * Print the value of the button
         */
        console.log(evt.default.value);
    });

##Documentation
###Option

* **id**: The id of the DOM-element
* **options**: Set of default options. This is an object with the following aatributes:
    - *min*: the minimum value of the knob
    - *max*: the maximum value of the knob
    - *value*: the start value of the knob

###Methods

* **obj**: Returns the DOM-element of the corresponding knob
* **getValue**: Get the current value of the knob 
* **setValue**: Set a new value to the knob 
* **trigger**: trigger the events

###Events

When rotating the knob or clicking it to set the value an event is fired.

* **knob-rotate**: Parses the value of the knob