var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');

gulp.task('scripts', function () {
    return gulp.src( 'src/npknob.js' )
        .pipe( uglify() )
        .pipe( rename( 'npknob.min.js' ) )
        .pipe( gulp.dest( 'build' ) )
});
